############################################
#
# v86d
# 
############################################

V86D_VERSION = 0.1.10
V86D_SOURCE = v86d-$(V86D_VERSION).tar.bz2
V86D_SITE = https://mirror.koddos.net/calculate-linux/source/v86d
V86D_LICENSE = GPLv2

# The configure script autodetects the host architecture, 
# and assumes the target architecture to be the same as the host,
# so we must use the target architecture flags provided 
# by Buildroot for proper cross-compilation. The x86emu 
# configure option should be 'n' for i386, and 'y' for x86-64.

X86EMU = $(if $(BR2_i386),n,y)

# v86d's configure script is not autoconf-based.
# GENTARGETS macro will be used rather than AUTOTARGETS.

define V86D_CONFIGURE_CMDS
	(cd $(@D) ; ./configure --with-debug=n --with-klibc=n --with-x86emu=$(X86EMU))
endef

# It is necessary to define __i386__ explicitly for successful compilation.

define V86D_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC) -D__i386__" LD="$(TARGET_LD)" -C $(@D) all
endef

define V86D_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/v86d $(TARGET_DIR)/sbin/v86d	
endef
###$(eval $(call GENTARGETS,package,v86d))
$(eval $(generic-package))
